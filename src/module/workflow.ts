//@ts-ignore
import Actor5e from "../../../systems/dnd5e/module/actor/entity.js"
//@ts-ignore
import Item5e  from "../../../systems/dnd5e/module/item/entity.js"
//@ts-ignore
import AbilityTemplate from "../../../systems/dnd5e/module/pixi/ability-template.js";
import { warn, debug, log, i18n, noDamageSaves, cleanSpellName, MESSAGETYPES, error, MQdefaultDamageType } from "../midi-qol";
import { selectTargets, showItemCard } from "./itemhandling";
import { broadcastData } from "./GMAction";
import { installedModules } from "./setupModules";
import { configSettings, checkBetterRolls, itemRollButtons, autoRemoveTargets } from "./settings.js";
import { getSelfTargetSet, createDamageList, processDamageRoll, untargetDeadTokens, getSaveMultiplierForItem, requestPCSave, applyTokenDamage, checkRange, checkIncapcitated, testKey } from "./utils"
import { config } from "process";
import { ConfigPanel } from "./apps/ConfigPanel.js";

export const shiftOnlyEvent = {shiftKey: true, altKey: false, ctrlKey: false, metaKey: false, type: ""};
export function noKeySet(event) { return !(event?.shiftKey || event?.ctrlKey || event?.altKey || event?.metaKey)}

export const WORKFLOWSTATES = {
  NONE : 0,
  ROLLSTARTED : 1,
  AWAITTEMPLATE: 2,
  TEMPLATEPLACED: 3,
  VALIDATEROLL: 4,
  PREAMBLECOMPLETE : 5,
  WAITFORATTACKROLL : 6,
  ATTACKROLLCOMPLETE: 7,
  WAITFORDAMGEROLL: 8,
  DAMAGEROLLCOMPLETE: 9,
  WAITFORSAVES: 10,
  SAVESCOMPLETE: 11,
  ALLROLLSCOMPLETE: 12,
  APPLYDYNAMICEFFECTS: 13,
  ROLLFINISHED: 14
};

const defaultRollOptions = {
  advantage: false,
  disadvantage: false,
  versatile: false,
  fastForward: false
};

export class Workflow {
  [x: string]: any;
  static _actions: {};
  static _workflows: {} = {};
  actor : Actor5e;
  item: Item5e;
  itemCardId : string;
  itemCardData: {};
  displayHookId: number;

  event: {shiftKey: boolean, altKey: boolean, ctrlKey: boolean, metaKey: boolean, type: string};
  capsLock: boolean;
  speaker: any;
  tokenId: string;
  targets: Set<Token>;
  placeTemlateHookId: number;

  _id: string;
  saveDisplayFlavor: string;
  showCard: boolean;
  get id() { return this._id}
  itemId: string;
  itemUUId: string;
  itemLevel: number;
  currentState: number;

  isCritical: boolean;
  isFumble: boolean;
  hitTargets: Set<Token>;
  attackRoll: Roll;
  attackAdvantage: boolean;
  attackDisadvantage: boolean;
  diceRoll: number;
  attackTotal: number;
  attackCardData: ChatMessage;
  attackRollHTML: HTMLElement | JQuery<HTMLElement>;
  noAutoAttack: boolean; // override attack roll for standard care
  
  hitDisplayData: any[];

  damageRoll: Roll;
  damageTotal: number;
  damageDetail: any[];
  damageRollHTML: HTMLElement | JQuery<HTMLElement>;
  damageCardData: ChatMessage;
  defaultDamageType: string;
  noAutoDamage: boolean; // override damage roll for damage rolls

  saves: Set<Token>;
  failedSaves: Set<Token>
  advantageSaves : Set<Token>;
  saveRequests: any;
  saveTimeouts: any;

  versatile: boolean;
  saveDisplayData;

  chatMessage: ChatMessage;
  hideTags: string[];
  displayId: string;

  static eventHack: any;
  
  get isBetterRollsWorkflow() {return false};

  static get workflows() {return Workflow._workflows}
  static getWorkflow(id:string):Workflow {
    debug("Get workflow ", id, Workflow._workflows,  Workflow._workflows[id])
    return Workflow._workflows[id];
  }

  static initActions(actions: {}) {
    Workflow._actions = actions;
  }

  public processAttackEventOptions(event) {
    this.rollOptions.fastForward = this.rollOptions.fastForward || (["all", "attack"].includes(configSettings.autoFastForward));
    if (configSettings.speedItemRolls && !this.isBetterRollsWorkflow) {
      this.rollOptions.advKey = this.rollOptions.advKey || testKey(configSettings.keyMapping["DND5E.Advantage"], event);
      this.rollOptions.disKey = this.rollOptions.disKey || testKey(configSettings.keyMapping["DND5E.Disadvantage"], event);
      this.rollOptions.versaKey = this.rollOptions.versaKey || testKey(configSettings.keyMapping["DND5E.Versatile"], event);
      this.rollOptions.advantage = this.rollOptions.advantage || this.rollOptions.advKey;
      this.rollOptions.disadvantage = this.rollOptions.disadvantage || this.rollOptions.disKey;
      this.rollOptions.fastForward = this.rollOptions.fastForward || ["all", "attack"].includes(configSettings.autoFastForward); // || this.rollOptions.advantage || this.rollOptions.disadvantage;
      this.rollOptions.fastForward = this.rollOptions.fastForward || this.rollOptions.advKey || this.rollOptions.disKey || false;
      /* TODO Look at thsi further
      if (["attack", "all"].includes(configSettings.autoFastForward) && advKey && disKey) { // don't need fastforward
        this.rollOptions.critical = true;
      }
      */
      this.rollOptions.versatile = this.rollOptions.versatile || (this.rollOptions.versaKey || false);
    } else {
      this.rollOptions.advKey = this.rollOptions.advKey || event?.altKey;
      this.rollOptions.disKey = this.rollOptions.disKey || event?.ctrlKey || event?.metaKey;
      // const versaKey = event?.shiftKey;
      this.rollOptions.fastForward = this.rollOptions.fastForward || (["all", "attack"].includes(configSettings.autoFastForward));
      this.rollOptions.fastForward = this.rollOptions.fastForward || (event?.altKey || event?.shfitKey || event?.ctrlKey);
      this.rollOptions.advantage = this.rollOptions.advKey;
      this.rollOptions.disadvantage = this.rollOptions.disKey;
    }
    const midiFlags = this.actor?.data.flags["midi-qol"];
    const advantage = midiFlags?.advantage;
    const disadvantage = midiFlags?.disadvantage;
    if (advantage) {
      const actType = this.item?.data.data?.actionType || "none"
      const withAdvantage = advantage.all || advantage.attack?.all || (advantage.attack && advantage.attack[actType]);
      this.rollOptions.advantage = this.rollOptions.advantage || withAdvantage;
      // if (withAdvantage) this.rollOptions.fastForward = true;
    }
    if (disadvantage) {
      const actType = this.item?.data.data?.actionType || "none"
      const withDisadvantage = disadvantage.all || disadvantage.attack?.all || (disadvantage.attack && disadvantage.attack[actType]);
      this.rollOptions.disadvantage = this.rollOptions.disadvantage || withDisadvantage;
      // if (withDisadvantage) this.rollOptions.fastForward = true;
    }
    if (this.capsLock) this.rollOptions.fastForward = true;
    if (this.rollOptions.advantage && this.rollOptions.disadvantage) {
      this.rollOptions.advantage = false;
      this.rollOptions.disadvantage = false;
      this.rollOptions.fastForward = true;
    }
    if (game.user.isGM) {
      this.rollOptions.fastForward = configSettings.gmAutoFastForwardAttack || this.rollOptions.advKey || this.rollOptions.disKey;
    }
  }
  public processDamageEventOptions(event) { 
    // trap workflows are fastforward by default.
    this.rollOptions.fastForward = this.__proto__.constructor.name === "TrapWorkflow";
    if (["all", "damage"].includes(configSettings.autoFastForward)) this.rollOptions.fastForward = true;
    // if we have an event here it means they clicked on the damage button?
    var critKey;
    var disKey;
    var advKey;
    var versaKey;
    var fastForwardKey;
    var noCritKey;
    if (configSettings.speedItemRolls && !this.isBetterRollsWorkflow) {
      disKey = testKey(configSettings.keyMapping["DND5E.Disadvantage"], event);
      critKey = testKey(configSettings.keyMapping["DND5E.Critical"], event);
      advKey = testKey(configSettings.keyMapping["DND5E.Advantage"], event);
      versaKey = testKey(configSettings.keyMapping["DND5E.Versatile"], event);
      noCritKey = disKey;
      fastForwardKey = (advKey && disKey) || this.capsLock;
    } else { // use default behaviour
      critKey = event?.altKey || event?.metaKey;
      noCritKey = event?.ctrlKey;
      fastForwardKey = (critKey && noCritKey);
    }

    if (fastForwardKey) critKey = false;
    if (fastForwardKey) { // fastforwarding roll
      this.rollOptions.critical = this.isCritical;
      this.rollOptions.fastForward = true;
    } else if ((!game.user.isGM && ["all", "damage"].includes(configSettings.autoFastForward)) ||
               (game.user.isGM && configSettings.gmAutoFastForwardDamage) ) {
      this.rollOptions.critical = this.rollOptions.critical || this.isCritical || critKey;
      if (noCritKey) {
        this.rollOptions.critical = false;
        this.isCritical = false;
      }
      this.rollOptions.fastForward = true;
    } else if ((!game.user.isGM && configSettings.autoRollDamage !== "none") || 
                (game.user.isGM && configSettings.gmAutoDamage !== "none") ) {
      this.rollOptions.critical = this.isCritical || this.rollOptions.critical || critKey;
    } else {
      this.rollOptions.critical = critKey && !noCritKey;
    }
    this.rollOptions.fastForward = this.rollOptions.fastForward || critKey || noCritKey || fastForwardKey 
      || (!game.user.isGM && ["all", "damage"].includes(configSettings.autoFastForward)) 
      || ( game.user.isGM && configSettings.gmAutoFastForwardAttack );
    this.rollOptions.versatile = this.rollOptions.versatile || versaKey;

    this.rollOptions.spellLevel = this.itemLevel;
    this.processCriticalFlags()
  }

  processCriticalFlags() {
/*
* flags.midi-qol.critical.all
* flags.midi-qol.critical.mwak/rwak/msak/rsak/other
* flags.midi-qol.noCritical.all
* flags.midi-qol.noCritical.mwak/rwak/msak/rsak/other
*/
    // check actor force critical/noCritical
    const criticalFlags = getProperty(this.actor.data, `flags.midi-qol.critical`) ?? {};
    const noCriticalFlags = getProperty(this.actor.data, `flags.midi-qol.noCritical`) ?? {};
    const attackType = this.item?.data.data.actionType;
    if (criticalFlags.all || criticalFlags[attackType]) this.rollOptions.critical = true;
    if (noCriticalFlags.all || noCriticalFlags[attackType]) this.rollOptions.critical = false;
    if (this.rollOptions.critical !== undefined) this.isCritical = this.rollOptions.critical;

    // check max roll
    const maxFlags = getProperty(this.actor.data, `flags.midi-qol.maxRoll`) ?? {};
    this.rollOptions.maxRoll = (maxFlags.all || maxFlags[attackType]) ?? false;

    // check target critical/nocritical
    if (this.targets.size !== 1) return;
    const firstTarget = this.targets.values().next().value;
    const grants = firstTarget.actor?.data.flags["midi-qol"]?.grants?.critical ?? {};
    const fails = firstTarget.actor?.data.flags["midi-qol"]?.fail?.critical ?? {};
    if (grants?.all || grants[attackType]) this.rollOptions.critical = true;
    if (fails?.all || fails[attackType]) this.rollOptions.critical = false;
    if (this.rollOptions.critical !== undefined) this.isCritical = this.rollOptions.critical;
  }

  checkAbilityAdvantage() {
    if (!["mwak", "rwak"].includes(this.item?.data.data.actionType)) return;
    const ability = this.item?.data.data.ability ?? "str";
    this.rollOptions.advantage |= getProperty(this.actor.data, `flags.midi-qol.advantage.attack.${ability}`);
    this.rollOptions.disadvantage |= getProperty(this.actor.data, `flags.midi-qol.disadvantage.attack.${ability}`);
    if (this.rollOptions.advantage && this.rollOptions.disadvantage) {
      this.rollOptions.advantage = false;
      this.rollOptions.disadvantage = false;
    }
  }

  checkTargetAdvantage() {
    if (!this.item) return;
    if (!this.targets?.size) return;
    const firstTarget = this.targets.values().next().value;
    const grants = firstTarget.actor?.data.flags["midi-qol"]?.grants;
    if (!grants) return;

    const actionType = this.item.data.data.actionType;
    if (!["rwak", "mwak", "rsak", "msak"].includes(actionType)) return;

    const attackAdvantage = grants.advantage?.attack || {};
    const grantsAdvantage = grants.all || attackAdvantage.all || attackAdvantage[actionType]
    const attackDisadvantage = grants.disadvantage?.attack || {};
    const grantsDisadvantage = grants.all || attackDisadvantage.all || attackDisadvantage[actionType]
    this.rollOptions.advantage = this.rollOptions.advantage || grantsAdvantage;
    this.rollOptions.disadvantage = this.rollOptions.disadvantage || grantsDisadvantage;
    if (this.rollOptions.advantage || this.rollOptions.disadvantage) this.rollOptions.fastForward = true;
    if (this.rollOptions.advantage && this.rollOptions.disadvantage) {
      this.rollOptions.advantage = false;
      this.rollOptions.disadvantage = false;
    }
  }

  constructor(actor: Actor5e, item: Item5e, tokenId, speaker, targets, event: any) {
    this.rollOptions = duplicate(defaultRollOptions);
    this.actor = actor;
    this.item = item;
    if (!this.item) {
      this.itemId = randomID();
      this.itemUUId = this.itemId;
    } else {
      this.itemId = item?.uuid;
      this.itemUUId = item?.uuid;
    }
    if (Workflow.getWorkflow(this.itemUUId)) {
      Workflow.removeWorkflow(this.itemUUId);
    }
    
    this.tokenId = tokenId ?? speaker.token;
    this.speaker = speaker;
    this.targets = targets; 
    this.saves = new Set();
    this.failedSaves = new Set(this.targets)
    this.hitTargets = new Set(this.targets);
    this.isCritical = false;
    this.isFumble = false;
    this.currentState = WORKFLOWSTATES.NONE;
    this.itemLevel = item?.level || 0;
    this._id = randomID();
    this.displayId = this.id;
    this.itemCardData = {};
    this.attackCardData = undefined;
    this.damageCardData = undefined;
    this.event = event;
    this.capsLock = event?.getModifierState && event.getModifierState("CapsLock");
    this.rollOptions = {disKey: false, advKey: false, versaKey: false, critKey: false, fastForward: false};
    if (this.item && !this.item.hasAttack) this.processDamageEventOptions(event);
    else this.processAttackEventOptions(event);
    

    this.saveRequests = {};
    this.saveTimeouts = {};

    this.placeTemlateHookId = null;
    this.damageDetail = [];
    this.hideTags = new Array();
    this.displayHookId = null;
    Workflow._workflows[this.itemUUId] = this;
  }

  public someEventKeySet() {
    return this.event?.shiftKey || this.event?.altKey || this.event?.ctrlKey || this.event?.metaKey;
     // || this.rollOptions.advantage || this.rollOptions.disadvantage || this.rollOptions.fastForward;
  }

  static removeWorkflow(id: string) {
    if (!Workflow._workflows[id]) warn ("removeWorkflow: No such workflow ", id)
    else {
      let workflow = Workflow._workflows[id];
      // If the attack roll broke and we did we roll again will have an extra hook laying around.
      if (workflow.displayHookId) Hooks.off("preCreateChatMessage", workflow.displayHookId);
      // This can lay around if the template was never placed.
      if (workflow.placeTemlateHookId) Hooks.off("createMeasuredTemplate", workflow.placeTemlateHookId)
      delete Workflow._workflows[id];
    }
  }

  async next(nextState: number) {
    setTimeout(() => this._next(nextState), 0); // give the rest of queued things a chance to happen
    // this._next(nextState);
  }

  async _next(newState: number) {
    this.currentState = newState;
    let state = Object.entries(WORKFLOWSTATES).find(a=>a[1]===newState)[0];
    warn("workflow.next ", state, this._id, this)
    switch (newState) {
      case WORKFLOWSTATES.NONE:
        debug(" workflow.next ", state, this.item, configSettings.autoTarget, this.item.hasAreaTarget);
        if (configSettings.autoTarget !== "none" && this.item.hasAreaTarget) {
          return this.next(WORKFLOWSTATES.AWAITTEMPLATE);
        }
        const targetDetails = this.item.data.data.target;
        if (configSettings.rangeTarget && targetDetails?.units === "ft" && ["creature", "ally", "enemy"].includes(targetDetails?.type)) {
          this.setRangedTargets(targetDetails)
          return this.next(WORKFLOWSTATES.TEMPLATEPLACED);
        }
        return this.next(WORKFLOWSTATES.VALIDATEROLL);

      case WORKFLOWSTATES.AWAITTEMPLATE:
        if (this.item.hasAreaTarget && configSettings.autoTarget !== "none") {
          debug("Item has template registering Hook");
          this.placeTemlateHookId = Hooks.once("createMeasuredTemplate", selectTargets.bind(this));
          return;
        }
        return this.next(WORKFLOWSTATES.TEMPLATEPLACED);

      case WORKFLOWSTATES.TEMPLATEPLACED:
        // Some modules stop being able to get the item card id.
        if (!this.itemCardId) return this.next(WORKFLOWSTATES.VALIDATEROLL);
        const chatMessage: ChatMessage = game.messages.get(this.itemCardId);
        // remove the place template button from the chat card.
        //@ts-ignore
        let content = chatMessage && duplicate(chatMessage.data.content)
        let buttonRe = /<button data-action="placeTemplate">[^<]*<\/button>/
        content = content?.replace(buttonRe, "");
        await chatMessage?.update({
          "content": content, 
          "flags.midi-qol.playSound": false, 
          "flags.midi-qol.type": MESSAGETYPES.ITEM, 
          type: CONST.CHAT_MESSAGE_TYPES.OTHER});
        return this.next(WORKFLOWSTATES.VALIDATEROLL);

      case WORKFLOWSTATES.VALIDATEROLL:
        // do pre roll checks
        if (configSettings.preRollChecks) {
          if (!checkRange(this.actor, this.item, null)) return this.next(WORKFLOWSTATES.ROLLFINISHED);
          if (!checkIncapcitated(this.actor, this.item, null)) return this.next(WORKFLOWSTATES.ROLLFINISHED);
        }
        return this.next(WORKFLOWSTATES.PREAMBLECOMPLETE);

      case WORKFLOWSTATES.PREAMBLECOMPLETE:
        return this.next(WORKFLOWSTATES.WAITFORATTACKROLL);
        break;

      case WORKFLOWSTATES.WAITFORATTACKROLL:
        if (!this.item.hasAttack) {
          return this.next(WORKFLOWSTATES.WAITFORDAMGEROLL);
        }
        if (this.noAutoAttack) return;
        let shouldRoll = this.someEventKeySet() || configSettings.autoRollAttack;
        if (game.user.isGM) 
          shouldRoll = configSettings.gmAutoAttack || this.someEventKeySet();
        if (shouldRoll) {
          this.processAttackEventOptions(event);
          warn("attack roll ", shouldRoll, this.rollOptions)
          this.item.rollAttack({event: {}});
        }
        return;

      case WORKFLOWSTATES.ATTACKROLLCOMPLETE:
        this.processAttackRoll();
        await this.displayAttackRoll(false, configSettings.mergeCard);
        if (configSettings.autoCheckHit !== "none") {
          await this.checkHits();
          const rollMode = game.settings.get("core", "rollMode");
          this.whisperAttackCard = configSettings.autoCheckHit === "whisper" || rollMode === "blindroll" || rollMode === "gmroll";
          await this.displayHits(this.whisperAttackCard, configSettings.mergeCard);
        }
        // We only roll damage on a hit. but we missed everyone so all over, unless we had no one targetted
        Hooks.callAll("midi-qol.AttackRollComplete", this);
        if (!game.user.isGM && (configSettings.autoRollDamage === "onHit" && this.hitTargets.size === 0 && this.targets.size !== 0)) return this.next(WORKFLOWSTATES.ROLLFINISHED);
        if (game.user.isGM && (configSettings.gmAutoDamage === "onHit" && this.hitTargets.size === 0 && this.targets.size !== 0)) return this.next(WORKFLOWSTATES.ROLLFINISHED);
        return this.next(WORKFLOWSTATES.WAITFORDAMGEROLL);

      case WORKFLOWSTATES.WAITFORDAMGEROLL:
        debug(`wait for damage roll has damaee ${this.item.hasDamage} isfumble ${this.isFumble} no auto damage ${this.noAutoDamage}`);
        if (!this.item.hasDamage) return this.next(WORKFLOWSTATES.WAITFORSAVES);
        if (this.isFumble && configSettings.autoRollDamage !== "none") {
          // Auto rolling damage but we fumbled - we failed - skip everything.
          return this.next(WORKFLOWSTATES.ROLLFINISHED);
        } 
        if (this.noAutoDamage) return; // we are emulating the standard card specially.
        let shouldRollDamage = configSettings.autoRollDamage === "always" 
                                || (configSettings.autoRollDamage !== "none" && !this.item.hasAttack)
                                || (configSettings.autoRollDamage === "onHit" && (this.hitTargets.size > 0 || this.targets.size === 0));
        if (game.user.isGM) {
          shouldRollDamage = configSettings.gmAutoDamage === "always" 
                                || (configSettings.gmAutoDamage !== "none" && !this.item.hasAttack)
                                || (configSettings.gmAutoDamage === "onHit" && (this.hitTargets.size > 0 || this.targets.size === 0));
        }
        debug("autorolldamage ", configSettings.autoRollDamage, " has attack ", this.item.hasAttack, " targets ", this.hitTargets)
        if (shouldRollDamage) {
          warn(" about to roll damage ", this.event, configSettings.autoRollAttack, configSettings.autoFastForward)
          debug("Rolling damage ", event, this.itemLevel, this.rollOptions.versatile);
          this.rollOptions.spellLevel = this.itemLevel;
          await this.item.rollDamage(this.rollOptions);
        }
        return; // wait for a damage roll to advance the state.

      case WORKFLOWSTATES.DAMAGEROLLCOMPLETE:
         if (configSettings.autoTarget === "none" && this.item.hasAreaTarget && !this.item.hasAttack) { 
           // we are not auto targeting so for area effect attacks, without hits (e.g. fireball)
          this.targets = new Set(game.user.targets);
          this.hitTargets = new Set(game.user.targets);
          warn(" damage roll complete for non auto target area effects spells", this)
        }
        Hooks.callAll("midi-qol.preDamageRollComplete", this)
        // apply damage to targets plus saves plus immunities
        // done here cause not needed for betterrolls workflow

        this.defaultDamageType = this.item.data.data.damage?.parts[0][1] || this.defaultDamageType || MQdefaultDamageType;
        if (this.item?.data.data.actionType === "heal") this.defaultDamageType = CONFIG.DND5E.healingTypes["healing"]; 
        this.damageDetail = createDamageList(this.damageRoll, this.item, this.defaultDamageType);
        /*
        this.criticalDetail = createDamageList(this.criticalRoll, this.item, this.defaultDamageType);
        */
        await this.displayDamageRoll(false, configSettings.mergeCard);
        if (this.isFumble) {
          return this.next(WORKFLOWSTATES.APPLYDYNAMICEFFECTS);
        }
        return this.next(WORKFLOWSTATES.WAITFORSAVES);

      case WORKFLOWSTATES.WAITFORSAVES:
        if (!this.item.hasSave) {
          this.saves = new Set(); // not auto checking assume no saves
          this.failedSaves = new Set(this.hitTargets);
          return this.next(WORKFLOWSTATES.SAVESCOMPLETE);
        }
        if (configSettings.autoCheckSaves !== "none") {
          //@ts-ignore ._hooks not defined
          debug("Check Saves: renderChat message hooks length ", Hooks._hooks["renderChatMessage"]?.length)
          // setup to process saving throws as generated
          let hookId = Hooks.on("renderChatMessage", this.processSaveRoll.bind(this));
          let brHookId = Hooks.on("renderChatMessage", this.processBetterRollsChatCard.bind(this));
          try {
            await this.checkSaves(true);
          } finally {
            //@ts-ignore - does not support ids
            Hooks.off("renderChatMessage", hookId);
            //@ts-ignore does not support ids
            Hooks.off("renderChatMessage", brHookId);
          }
          //@ts-ignore ._hooks not defined
          debug("Check Saves: renderChat message hooks length ", Hooks._hooks["renderChatMessage"]?.length)
          this.displaySaves(configSettings.autoCheckSaves === "whisper", configSettings.mergeCard);
        } else {// has saves but we are not checking so do nothing with the damage
          return this.next(WORKFLOWSTATES.ROLLFINISHED)
        }
        return this.next(WORKFLOWSTATES.SAVESCOMPLETE);

      case WORKFLOWSTATES.SAVESCOMPLETE:
        return this.next(WORKFLOWSTATES.ALLROLLSCOMPLETE);
  
      case WORKFLOWSTATES.ALLROLLSCOMPLETE:
        debug("all rolls complete ", this.damageDetail)

        if (this.damageDetail.length) processDamageRoll(this, this.damageDetail[0].type)
        Hooks.callAll("midi-qol.DamageRollComplete", this)
        return this.next(WORKFLOWSTATES.APPLYDYNAMICEFFECTS);

      case WORKFLOWSTATES.APPLYDYNAMICEFFECTS:
        // no item, not auto effects or not module skip
        if (!this.item || !configSettings.autoItemEffects) return this.next(WORKFLOWSTATES.ROLLFINISHED);
        const hasDAE = installedModules.get("dae") && (this.item?.effects?.entries.some(ef => ef.data.transfer === false));
        // no dynamiceffects skip
        this.applicationTargets = new Set();
        if (this.item.hasSave) this.applicationTargets = this.failedSaves;
        else if (this.item.hasAttack) this.applicationTargets = this.hitTargets;
        else this.applicationTargets = this.targets;
        if (hasDAE) {
          //@ts-ignore
          let dae = window.DAE;
          dae.doEffects(this.item, true, this.applicationTargets, {whisper: false, spellLevel: this.itemLevel, damageTotal: this.damageTotal, critical: this.isCritical, fumble: this.isFumble, itemCardId: this.itemCardId})
        }
        return this.next(WORKFLOWSTATES.ROLLFINISHED);

      case WORKFLOWSTATES.ROLLFINISHED:
        warn('Inside workflow.rollFINISHED');
        if (configSettings.allowUseMacro) {
          const macro = getProperty(this.item, "data.flags.midi-qol.onUseMacroName");
          let macroCommand;
          if (macro) {
            if (macro === "ItemMacro" && getProperty(this.item.data.flags, "itemacro.macro")) {
              const itemMacro = getProperty(this.item.data.flags, "itemacro.macro");
              macroCommand =  await CONFIG.Macro.entityClass.create({
                name: "MQOL-Item-Macro",
                type: "script",
                img: null,
                command: itemMacro.data.command,
                flags: { "dnd5e.itemMacro": true }
              }, { displaySheet: false, temporary: true });
            }
            else macroCommand = game.macros.getName(macro);
          }
          if (macroCommand) {
            let targets = [];
            let failedSaves = [];
            let hitTargets = [];
            let saves = [];
            for (let target of this.targets) targets.push(target.data);
            for (let save of this.saves) saves.push(save.data);
            for (let hit of this.hitTargets) hitTargets.push(hit.data);
            for (let failed of this.failedSaves) failedSaves.push(failed.data);
            const macroData = {
              actor: this.actor.data,
              tokenId: this.tokenId,
              item: this.item.data,
              targets,
              hitTargets,
              saves,
              failedSaves,
              damageRoll: this.damageRoll,
              attackRoll: this.attackRoll,
              itemCardId: this.itemCardId,
              isCritical: this.isCritical,
              isFumble: this.isFumble,
              spellLevel: this.itemLevel,
              damageTotal: this.damageTotal,
              damageDetail: this.damageDetail
            };
            warn("macro data ", macroData)
            //@ts-ignore -uses furnace macros which support arguments
            macroCommand.execute(macroData);
          }
        }

        // expire any effects on the actor that require it
        const myExpiredEffects = this.actor.effects.filter(ef => 
          ef.data.flags?.dae?.specialDuration === "1Action" ||
          (ef.data.flags?.dae?.specialDuration === "1Attack" && this.item?.hasAttack) ||
          (ef.data.flags?.dae?.specialDuration === "1Hit" && this.hitTargets.size > 0)
        ).map(ef=>ef.id);
        warn("1 off expiry ", myExpiredEffects);
        (myExpiredEffects?.length > 0) && await this.actor?.deleteEmbeddedEntity("ActiveEffect", myExpiredEffects);

        // expire effects on targeted tokens as required
        for (let target of this.targets) {
          //@ts-ignore effects
          const expiredEffects = target.actor?.effects?.filter(ef => {
            const wasAttacked = this.item?.hasAttack;
            const wasDamaged = this.item?.hasDamage && this.applicationTargets?.has(target);
            return (ef.data.flags?.dae?.specialDuration === "isAttacked" && wasAttacked) ||
                   (ef.data.flags?.dae?.specialDuration === "isDamaged" && wasDamaged) 
          }).map(ef=> ef.id);
          if (expiredEffects?.length > 0) {
            const intendedGM = game.user.isGM ? game.user : game.users.entities.find(u => u.isGM && u.active);
            if (!intendedGM) {
              ui.notifications.error(`${game.user.name} ${i18n("midi-qol.noGM")}`);
              error("No GM user connected - cannot remove effects");
              return;
            }
            broadcastData({
              action: "removeEffects",
              tokenId: target.id,
              effects: expiredEffects,
              intendedFor: intendedGM.id
            });
          } // target.actor?.deleteEmbeddedEntity("ActiveEffect", expiredEffects);
        }

        delete Workflow._workflows[this.itemId];
        Hooks.callAll("minor-qol.RollComplete", this); // just for the macro writers.
        Hooks.callAll("midi-qol.RollComplete", this);
        if (autoRemoveTargets !== "none") setTimeout(untargetDeadTokens, 500); // delay to let the updates finish

        // disable sounds for when the chat card might be reloaed.
        if (this.__proto__.constructor.name !== "BetterRollsWorkflow") {
          let itemCard = game.messages.get(this.itemCardId);
          let waitForDiceSoNice = configSettings.mergeCard && (this.item?.hasAttck || this.item?.hasDamage || this.item?.hasSaves);
          waitForDiceSoNice = waitForDiceSoNice && game.dice3d?.messageHookDisabled && game.dice3d?.isEnabled();

          await itemCard?.update({
            "flags.midi-qol.playSound": false, 
            "flags.midi-qol.type": MESSAGETYPES.ITEM, 
            type: CONST.CHAT_MESSAGE_TYPES.OTHER,
            "flags.midi-qol.waitForDiceSoNice": waitForDiceSoNice,
            "flags.midi-qol.hideTag": this.hideTags,
            "flags.midi-qol.displayId": this.displayId
          });
          
          setTimeout(() => {
            // remove hide tags after a bit
            itemCard?.update({"flags.midi-qol.hideTag": []});
          },4000)
          
        }
        //@ts-ignore ui.chat undefined.
        ui.chat.scrollBottom();
        return;
    }
  }

  async displayAttackRoll(whisper = false, doMerge) {
    const chatMessage: ChatMessage = game.messages.get(this.itemCardId);
    //@ts-ignore content not definted
    let content = chatMessage && duplicate(chatMessage.data.content)
    let buttonRe = /<button data-action="attack">[^<]*<\/button>/
    content = content?.replace(buttonRe, "");
    var rollSound =  configSettings.diceSound;
    const flags = chatMessage?.data.flags || {};
    let newFlags = {};
    

    if (doMerge && chatMessage) { // display the attack roll
      let searchString = '<div class="midi-qol-attack-roll"></div>';
      const attackString = this.rollOptions.advantage ? i18n("DND5E.Advantage") : this.rollOptions.disadvantage ? i18n("DND5E.Disadvantage") : i18n("DND5E.Attack")
      let replaceString = `<div style="text-align:center" >${attackString}<div class="midi-qol-attack-roll">${this.attackRollHTML}</div></div>`
      content = content.replace(searchString, replaceString);
      if ( this.attackRoll.dice.length ) {
        const d = this.attackRoll.dice[0];
        const isD20 = (d.faces === 20);
        if (isD20 ) {
          // Highlight successes and failures
          if ( d.options.critical && (d.total >= d.options.critical) ) {
            content = content.replace('dice-total', 'dice-total critical');
          } 
          else if ( d.options.fumble && (d.total <= d.options.fumble) ) {
            content = content.replace('dice-total', 'dice-total fumble');
          }
          else if ( d.options.target ) {
            if ( this.attackRoll.total >= d.options.target ) content = content.replace('dice-total', 'dice-total success');
            else content = content.replace('dice-total', 'dice-total failure');
          }
        }
      }
      if (!!!game.dice3d?.messageHookDisabled) this.hideTags = [".midi-qol-attack-roll", "midi-qol-damage-roll"];
      warn("Display attack roll ", this.attackCardData, this.attackRoll)
      newFlags = mergeObject(flags, {
          "midi-qol": 
          {
            type: MESSAGETYPES.ATTACK,
            waitForDiceSoNice: true,
            hideTag: this.hideTags,
            playSound: true,
            roll: this.attackRoll.roll,
            // roll: this.attackCardData.roll,
            displayId: this.displayId,
            isCritical: this.isCritical,
            isFumble: this.isFumble,
            isHit: this.isHit,
            sound: rollSound
          }
        }, {overwrite: true, inplace: false}
      )
    }
    await chatMessage?.update({"content": content, flags: newFlags });
  }

  async displayDamageRoll(whisper = false, doMerge) {
    let chatMessage: ChatMessage = game.messages.get(this.itemCardId);
    //@ts-ignore content not definted 
    let content = chatMessage && duplicate(chatMessage.data.content)
    const versatileRe = /<button data-action="versatile">[^<]*<\/button>/
    const damageRe = /<button data-action="damage">[^<]*<\/button>/
    content = content?.replace(damageRe, "")
    content = content?.replace(versatileRe, "<div></div>")
    var rollSound = configSettings.diceSound;
    var newFlags = chatMessage?.data.flags || {};
    if (doMerge && chatMessage) {
      const searchString = '<div class="midi-qol-damage-roll"></div>';
      const damageString = `(${this.item?.data.data.damage.parts
          .map(a=>(CONFIG.DND5E.damageTypes[a[1]] || this.defaultDamageType || MQdefaultDamageType)).join(",") 
            || this.defaultDamageType || MQdefaultDamageType})`;

      //@ts-ignore .flavor not defined
      const dmgHeader = configSettings.mergeCardCondensed ? damageString : (this.flavor ?? damageString);
      let replaceString = `<div class="midi-qol-damage-roll"><div style="text-align:center" >${dmgHeader}${this.damageRollHTML || ""}</div></div>`
      content = content.replace(searchString, replaceString);
      if (!!!game.dice3d?.messageHookDisabled) {
        if (configSettings.autoRollDamage  === "none" || !["all","damage"].includes(configSettings.autoFastForward)) {
          // not auto rolling damage so hits will have been long displayed
          this.hideTags = [".midi-qol-damage-roll"]
        } else this.hideTags.push(".midi-qol-damage-roll");
      }
      this.displayId = randomID();
      newFlags = mergeObject(newFlags, {
        "midi-qol": {
          waitForDiceSoNice: true,
          type: MESSAGETYPES.DAMAGE,
          playSound: false, 
          sound: rollSound,
          // roll: this.damageCardData.roll,
          roll: this.damageRoll.roll,
          damageDetail: this.damageDetail,
          damageTotal: this.damageTotal,
          hideTag: this.hideTags,
          displayId: this.displayId
        }
      }, {overwrite: true, inplace: false});
    }
    await chatMessage?.update( {"content": content, flags: newFlags});
  }

  async displayHits(whisper = false, doMerge) {
    const templateData = {
      attackType: this.item.name,
      oneCard: configSettings.mergeCard,
      hits: this.hitDisplayData,
      isCritical: this.isCritical, 
      isGM: game.user.isGM,
    }
    warn("displayHits ", templateData, whisper, doMerge);
    const hitContent = await renderTemplate("modules/midi-qol/templates/hits.html", templateData) || "No Targets";
    const chatMessage: ChatMessage = game.messages.get(this.itemCardId);

    if(doMerge && chatMessage) {
      // @ts-ignore .content not defined
      var content = chatMessage && duplicate(chatMessage.data.content);    
      var searchString;
      var replaceString;
      if (!!!game.dice3d?.messageHookDisabled) this.hideTags.push(".midi-qol-hits-display")
      switch (this.__proto__.constructor.name) {
        case "BetterRollsWorkflow":
          searchString =  '<footer class="card-footer">';
          replaceString = `<div class="midi-qol-hits-display">${hitContent}</div><footer class="card-footer">`;
          content = content.replace(searchString, replaceString);
          await chatMessage.update({
            "content": content, 
            "flags.midi-qol.playSound": false,
            "flags.midi-qol.type": MESSAGETYPES.HITS,
            type: CONST.CHAT_MESSAGE_TYPES.OTHER,
            "flags.midi-qol.waitForDiceSoNice": false,
            "flags.midi-qol.hideTag": "",
            "flags.midi-qol.displayId": this.displayId
          });
          break;
        case "Workflow":
        case "TrapWorkflow":
          searchString =  '<div class="midi-qol-hits-display"></div>';
          replaceString = `<div class="midi-qol-hits-display">${hitContent}</div>`
          content = content.replace(searchString, replaceString);
          await chatMessage.update({
            "content": content, 
            "flags.midi-qol.playSound": this.isCritical || this.isFumble, 
            "flags.midi-qol.type": MESSAGETYPES.HITS,
            type: CONST.CHAT_MESSAGE_TYPES.OTHER,
            "flags.midi-qol.waitForDiceSoNice": true,
            "flags.midi-qol.hideTag": this.hideTags,
            "flags.midi-qol.displayId": this.displayId,
            "flags.midi-qol.sound": this.isCritical ? configSettings.criticalSound : configSettings.fumbleSound
          });
          break;
        }
    } else {
      let speaker = ChatMessage.getSpeaker();
      speaker.alias = (configSettings.useTokenNames && speaker.token) ? canvas.tokens.get(speaker.token).name : speaker.alias;

      if (game.user.targets.size > 0) {
        let chatData: any = {
          user: game.user,
          speaker,
          content: hitContent || "No Targets",
          type: CONST.CHAT_MESSAGE_TYPES.OTHER,
        }
        const rollMode = game.settings.get("core", "rollMode");
        if (whisper || rollMode !== "roll") 
        {
          chatData.whisper = ChatMessage.getWhisperRecipients("GM").filter(u=>u.active);
          chatData.user = ChatMessage.getWhisperRecipients("GM").find(u=>u.active);
          if (rollMode === "blindroll") {
            chatData["blind"] = true;
          }

          debug("Trying to whisper message", chatData)
        }
        if (this.__proto__.constructor.name !== "BetterRollsWorkflow") {
            setProperty(chatData, "flags.midi-qol.waitForDiceSoNice", true);
            if (!whisper) setProperty(chatData, "flags.midi-qol.hideTag", "midi-qol-hits-display")
        } else { // better rolls workflow
            setProperty(chatData, "flags.midi-qol.waitForDiceSoNice", false);
            // setProperty(chatData, "flags.midi-qol.hideTag", "")
        }
        ChatMessage.create(chatData);
      }
    }
  }

  async displaySaves(whisper, doMerge) {
    let chatData: any = {};
    const noDamage = getSaveMultiplierForItem(this.item) === 0 ? i18n("midi-qol.noDamage") : "";
    let templateData = {
      noDamage,
      saves: this.saveDisplayData, 
        // TODO force roll damage
    }
    const chatMessage: ChatMessage = game.messages.get(this.itemCardId);
    const saveContent = await renderTemplate("modules/midi-qol/templates/saves.html", templateData);
    if (doMerge && chatMessage) {
        // @ts-ignore .content not defined
        let content = duplicate(chatMessage.data.content)
        var searchString;
        var replaceString;
        const saveFlavor = configSettings.displaySaveDC ? this.saveDisplayFlavor : `${CONFIG.DND5E.abilities[this.item.data.data.save.ability]} ${i18n("midi-qol.saving-throws")}`;
        const saveHTML = `<div class="midi-qol-nobox midi-qol-bigger-text">${saveFlavor}</div>`;
        if (!!!game.dice3d?.messageHookDisabled) this.hideTags = [".midi-qol-saves-display"];
        switch (this.__proto__.constructor.name) {
          case "BetterRollsWorkflow":
            searchString =  '<footer class="card-footer">';
            replaceString = `<div data-item-id="${this.item._id}"></div><div class="midi-qol-saves-display">${saveHTML}${saveContent}</div><footer class="card-footer">`
            content = content.replace(searchString, replaceString);
            await chatMessage.update({
              "content": content, 
              type: CONST.CHAT_MESSAGE_TYPES.OTHER,
              "flags.midi-qol.type": MESSAGETYPES.SAVES,
              "flags.midi-qol.hideTag": this.hideTags
            });

            //@ts-ignore
            chatMessage.data.content = content;
          break;
        case "Workflow":
        case "TrapWorkflow":
            searchString =  '<div class="midi-qol-saves-display"></div>';
            // replaceString = `<div data-item-id="${this.item._id}"></div><div class="midi-qol-saves-display"><div class="midi-qol-nobox midi-qol-bigger-text">${saveFlavor}</div>${saveContent}</div>`
            replaceString = `<div data-item-id="${this.item._id}"></div><div class="midi-qol-saves-display">${saveHTML}${saveContent}</div><footer class="card-footer">`
            content = content.replace(searchString, replaceString);
            await chatMessage.update({
              "content": content, 
              type: CONST.CHAT_MESSAGE_TYPES.OTHER,
              "flags.midi-qol.type": MESSAGETYPES.SAVES,
              "flags.midi-qol.hideTag": this.hideTags
            });
            //@ts-ignore
            chatMessage.data.content = content;
        }
    } else {
      const gmUser = game.users.find((u: User) => u.isGM && u.active);
      //@ts-ignore
      let speaker = ChatMessage._getSpeakerFromUser({user: gmUser});
      chatData = {
        user: gmUser._id,
        speaker,
        content: `<div data-item-id="${this.item._id}"></div> ${saveContent}`,
        flavor: `<h4>${this.saveDisplayFlavor}</h4>`, 
        type: CONST.CHAT_MESSAGE_TYPES.OTHER,
        flags: { "midi-qol": {type: MESSAGETYPES.SAVES, waitForDiceSoNice: true}}
      };

      const rollMode = game.settings.get("core", "rollMode");
      if (configSettings.autoCheckSaves === "whisper" || whisper || rollMode !== "roll") 
      {
        chatData.whisper = ChatMessage.getWhisperRecipients("GM").filter(u=>u.active);
        chatData.user = ChatMessage.getWhisperRecipients("GM").find(u=>u.active);
        if (rollMode === "blindroll") {
          chatData["blind"] = true;
        }

        debug("Trying to whisper message", chatData)
      }
      await ChatMessage.create(chatData);
    }
  }

  playerFor(target: Token) {
    // find the controlling player
    let player = game.users.players.find(p => p.character?._id === target.actor._id);
    if (!player?.active) { // no controller - find the first owner who is active
      //@ts-ignore permissions not defined
      player = game.users.players.find(p => p.active && target.actor.data.permission[p._id] === CONST.ENTITY_PERMISSIONS.OWNER)
      //@ts-ignore permissions not defined
      if (!player) player = game.users.players.find(p => p.active && target.actor.data.permission.default === CONST.ENTITY_PERMISSIONS.OWNER)
    }
    return player;
  }

/**
 * update this.saves to be a Set of successful saves from the set of tokens this.hitTargets and failed saves to be the complement
 */
  async checkSaves(whisper = false) {
    this.saves = new Set();
    this.failedSaves = new Set()
    this.advantageSaves = new Set();
    this.saveDisplayData = [];
    debug(`checkSaves: whisper ${whisper}  hit targets ${this.hitTargets}`)

    if (this.hitTargets.size <= 0) {
      this.saveDisplayFlavor = `<span>${i18n("midi-qol.noSaveTargets")}</span>`
      return;
    }
    let rollDC = this.item.data.data.save.dc;
    if (this.item.getSaveDC) {
      rollDC = this.item.getSaveDC()
    }
    let rollAbility = this.item.data.data.save.ability;
  
    let promises = [];
    // make sure saving throws are renabled.
    try {
      for (let target of this.hitTargets) {
        if (!target.actor) continue;  // no actor means multi levels or bugged actor - but we won't roll a save
        let advantage = false;
        // If spell, check for magic resistance
        if (this.item.data.type === "spell") {
          // check magic resistance in custom damage reduction traits
          advantage = (target?.actor?.data?.data?.traits?.dr?.custom || "").includes(i18n("midi-qol.MagicResistant"));
          // check magic resistance as a feature (based on the SRD name as provided by the DnD5e system)
          //@ts-ignore - data.data.items not defined in types
          advantage = advantage || (target?.actor?.data?.items?.filter(a => a.type==="feat" && a.name===i18n("midi-qol.MagicResistanceFeat"))?.length > 0);
          if (advantage) this.advantageSaves.add(target);
          debug(`${target.actor.name} resistant to magic : ${advantage}`);
        }

        let event = {};
        // TODO need to modify this according to speed rolls settings
        if (advantage) event = {shiftKey: false, ctlKey: false, altKey: true, metaKey: false}
        else event = {shiftKey: true, ctlKey: false, altKey: false, metaKey: false}

        if (configSettings.playerRollSaves !== "none") { // find a player to send the request to
          var player = this.playerFor(target);
          //@ts-ignore
          if (!player) player = ChatMessage.getWhisperRecipients("GM").find(u=>u.active);
        }
        if ((configSettings.playerRollSaves !== "none" && player?.active && !player?.isGM) || (configSettings.rollNPCSaves ?? "auto") !== "auto" ) { 
          warn(`Player ${player?.name} controls actor ${target.actor.name} - requesting ${CONFIG.DND5E.abilities[this.item.data.data.save.ability]} save`);
          promises.push(new Promise((resolve, reject) => {
            const eventToUse = duplicate(event);
            const advantageToUse = advantage;
            let requestId = target.actor.id;
            const playerName = player.name;
            const playerId = player._id;
            if (["letem", "letmeQuery"].includes(configSettings.playerRollSaves) && installedModules.get("lmrtfy")) requestId = randomID();
            this.saveRequests[requestId] = resolve;
            
            requestPCSave(this.item.data.data.save.ability, player, target.actor.id, advantage, this.item.name, rollDC, requestId)

            // set a timeout for taking over the roll
            this.saveTimeouts[requestId] = setTimeout(async () => {
              if (this.saveRequests[requestId]) {
                  delete this.saveRequests[requestId];
                  delete this.saveTimeouts[requestId];
                  //@ts-ignore actor.rollAbilitySave
                  let result = await target.actor.rollAbilitySave(this.item.data.data.save.ability, {messageData: { user: playerId }, advantage: advantageToUse, fastForward: true});
                  resolve(result);
              }
            }, (configSettings.playerSaveTimeout || 1) * 1000);
          }))
        } else {  // GM to roll save
          let showRoll = configSettings.autoCheckSaves === "allShow";
          // Find a player owner for the roll if possible
          let owner = this.playerFor(target);
          if (owner) showRoll = true; // Always show player save rolls
          // If no player owns the token, find an active GM
          if (!owner) owner = game.users.find((u: User) => u.isGM && u.active);
          // Fall back to rolling as the current user
          if (!owner) owner = game.user;
          //@ts-ignore actor.rollAbilitySave
          promises.push(target.actor.rollAbilitySave(this.item.data.data.save.ability, { messageData: {user: owner._id}, chatMessage: showRoll,  mapKeys: false, advantage, fastForward: true}));
        }
      }
    } catch (err) {
        console.warn(err)
    } finally {
    }
    debug("check saves: requests are ", this.saveRequests)
    var results = await Promise.all(promises);
    this.saveResults = results;

    let i = 0;
    for (let target of this.hitTargets) {
      if (!target.actor) continue; // these were skipped when doing the rolls so they can be skipped now
      if (!results[i]) error("Token ", target, "could not roll save assuming 0") 
      let rollTotal = results[i]?.total || 0;
      let saved = rollTotal >= rollDC;
      if (rollTotal >= rollDC) this.saves.add(target);
      else this.failedSaves.add(target);

      if (game.user.isGM) log(`Ability save: ${target.name} rolled ${rollTotal} vs ${rollAbility} DC ${rollDC}`);
      let saveString = i18n(saved ? "midi-qol.save-success" : "midi-qol.save-failure");
      let adv = this.advantageSaves.has(target) ? `(${i18n("DND5E.Advantage")})` : "";
      let img = target.data.img || target.actor.img;
      if ( VideoHelper.hasVideoExtension(img) ) {
        //@ts-ignore - createThumbnail not defined
        img = await game.video.createThumbnail(img, {width: 100, height: 100});
      }
      //@ts-ignore
      let isPlayerOwned = target.actor.hasPlayerOwner;
      if (isNewerVersion("0.6.9", game.data.version)) isPlayerOwned = target.actor.isPC
      this.saveDisplayData.push({
        name: target.name, 
        img, 
        isPC: isPlayerOwned, 
        target, 
        saveString, 
        rollTotal, 
        id: target.id, 
        adv
      });
      i++;
    }

    this.saveDisplayFlavor = `${this.item.name} DC ${rollDC} ${CONFIG.DND5E.abilities[rollAbility]} ${i18n(this.hitTargets.size > 1 ? "midi-qol.saving-throws" : "midi-qol.saving-throw")}:`;
  }

  processSaveRoll = (message, html, data) => {
    const isLMRTFY = (installedModules.get("lmrtfy") && message.data.flags?.lmrtfy?.data);
    if (!isLMRTFY && message.data.flags?.dnd5e?.roll?.type !== "save") return true;
    const requestId =  isLMRTFY ? message.data.flags.lmrtfy.data : message.data?.speaker?.actor;
    warn("processSaveToll", isLMRTFY, requestId, this.saveRequests )

    if (!requestId) return true;
    if (!this.saveRequests[requestId]) return true;
    const total = message._roll._total;
    const formula = message._roll._formula;
    if (this.saveRequests[requestId]) {
      clearTimeout(this.saveTimeouts[requestId]);
      this.saveRequests[requestId]({total, formula})
      delete this.saveRequests[requestId];
      delete this.saveTimeouts[requestId];
    }      
    return true;
  }

  processBetterRollsChatCard (message, html, data) {
    if (!checkBetterRolls && message?.data?.content?.startsWith('<div class="dnd5e red-full chat-card"'))  return;
    debug("processBetterRollsChatCard", message. html, data)
    const requestId = message.data.speaker.actor;
    if (!this.saveRequests[requestId]) return true;
    const title = html.find(".item-name")[0]?.innerHTML
    if (!title) return true;
    if (!title.includes("Save")) return true;
    const formula = "1d20";
    const total = html.find(".dice-total")[0]?.innerHTML;
    clearTimeout(this.saveTimeouts[requestId]);
    this.saveRequests[requestId]({total, formula})
    delete this.saveRequests[requestId];
    delete this.aveTimeouts[requestId];
    return true;
  }

  processAttackRoll() {
    //@ts-ignore
    this.diceRoll = this.attackRoll.results[0];
    //@ts-ignore
    this.diceRoll = this.attackRoll.terms[0].results.find(d => d.active).result;
    //@ts-ignore .terms undefined
    this.isCritical = this.diceRoll  >= this.attackRoll.terms[0].options.critical;
    //@ts-ignore
    //@ts-ignore .terms undefined
    this.isFumble = this.diceRoll <= this.attackRoll.terms[0].options.fumble;
    this.attackTotal = this.attackRoll.total;
    debug("processItemRoll: ", this.diceRoll, this.attackTotal, this.isCritical, this.isFumble)
  }

  async checkHits() {
    let isHit = true;

    let actor = this.actor;
    let item = this.item;
    
    // check for a hit/critical/fumble
    this.hitTargets = new Set();
    this.hitDisplayData = [];
  
    if (item?.data.data.target?.type === "self") {
      this.processCriticalFlags();
      this.targets = new Set([canvas.tokens.get(this.tokenId)]); //TODO check this is right
      debug("Check hits - self target")

    } else for (let targetToken of this.targets) {
      isHit = false;
      let targetName = configSettings.useTokenNames && targetToken.name ? targetToken.name : targetToken.actor?.name;
      let targetActor = targetToken.actor;
      if (!targetActor) continue; // tokens without actors are an abomination and we refuse to deal with them.
      let targetAC = targetActor.data.data.attributes.ac.value;
      if (!this.isFumble && !this.isCritical) {
          // check to see if the roll hit the target
          // let targetAC = targetActor.data.data.attributes.ac.value;
          isHit = this.attackTotal >= targetAC;
          if (isHit) this.processCriticalFlags();
      }

      if (game.user.isGM) log(`${this.speaker.alias} Rolled a ${this.attackTotal} to hit ${targetName}'s AC of ${targetAC} is hit ${isHit || this.isCritical}`);
      // Log the hit on the target
      let attackType = ""; //item?.name ? i18n(item.name) : "Attack";
      let hitString = this.isCritical ? i18n("midi-qol.criticals") : this.isFumble? i18n("midi-qol.fumbles") : isHit ? i18n("midi-qol.hits") : i18n("midi-qol.misses");
      let img = targetToken.data?.img || targetToken.actor.img;
      if ( VideoHelper.hasVideoExtension(img) ) {
        //@ts-ignore
        img = await game.video.createThumbnail(img, {width: 100, height: 100});
      }
      if (isNewerVersion("0.6.9", game.data.version)) 
        this.hitDisplayData.push({isPC: targetToken.actor.isPC, target: targetToken, hitString, attackType, img});
      else      
        //@ts-ignore hasPlayerOwner
        this.hitDisplayData.push({isPC: targetToken.actor.hasPlayerOwner, target: targetToken, hitString, attackType, img});
  
      // If we hit and we have targets and we are applying damage say so.
      if (isHit || this.isCritical) this.hitTargets.add(targetToken);
    }
  }

  setRangedTargets(targetDetails) {
    const speaker = ChatMessage.getSpeaker();
    const token = canvas.tokens.get(speaker.token);
    if (!token) {
      ui.notifications.warn(`${game.i18n.localize("midi-qol.noSelection")}`)
      return true;
    }
    // We have placed an area effect template and we need to check if we over selected
    let dispositions = targetDetails.type === "creature" ? [-1,0,1] : targetDetails.type === "ally" ? [token.data.disposition] : [-token.data.disposition];
    // release current targets
    game.user.targets.forEach(t => {
      //@ts-ignore
      t.setTarget(false, { releaseOthers: false });
    });
    game.user.targets.clear();
    // calculate pixels eqivalent distance - requires map to be in ft.
    let minDist = targetDetails.value * canvas.grid.size / canvas.scene.data.gridDistance;
    canvas.tokens.placeables
        .filter(target => target.actor && target.actor.data.data.details.race !== "trigger"
                          && target.actor.id !== token.actor.id
                          && dispositions.includes(target.data.disposition) 
                          && Math.hypot(token.center.x - target.center.x, token.center.y - target.center.y) <= minDist)
        .forEach(token=> {
          token.setTarget(true, { user: game.user, releaseOthers: false });
          game.user.targets.add(token);
    });
    this.targets = new Set(game.user.targets);
    this.saves = new Set();
    this.failedSaves = new Set(this.targets)
    this.hitTargets = new Set(this.targets);
  }
}

export class DamageOnlyWorkflow extends Workflow {
  constructor(actor: Actor5e, token: Token, damageTotal: number, damageType: string, targets: [Token], roll: Roll, 
        options: {flavor: string, itemCardId: string}) {

    super(actor, null, token, ChatMessage.getSpeaker(), new Set(targets), shiftOnlyEvent)
    this.damageTotal = damageTotal;
    this.damageDetail = [{type: damageType,  damage: damageTotal}];
    this.damageRoll = roll;
    this.flavor = options.flavor;
    this.defaultDamageType = CONFIG.DND5E.damageTypes[damageType] || damageType;
    // this.targets = new Set(targets);
    this.itemCardId = options.itemCardId;
    warn("Damage only workflow data", options, this)
    this.next(WORKFLOWSTATES.NONE);
    return this;
  }

  async _next(newState) {
    this.currentState = newState;
    warn("Newstate is ", newState)
    // let state = Object.entries(WORKFLOWSTATES).find(a=>a[1]===this.currentState)[0];
    switch(newState) {
      case WORKFLOWSTATES.NONE:
        if (configSettings.mergeCard && this.itemCardId) {
          this.damageRollHTML = await this.damageRoll.render();
          this.damageCardData = {
            //@ts-ignore
            flavor: "damage flavor",
            roll: this.damageRoll,
            speaker: this.speaker
          }
          await this.displayDamageRoll(false, configSettings.mergeCard && this.itemCardId)
        } else this.damageRoll.toMessage({flavor: this.flavor});
        this.hitTargets = new Set(this.targets);
        debug("DamageOnlyWorkflow.next ", newState, configSettings.speedItemRolls, this);
        warn("DamageOnlyWorkflow.next ", this.damageDetails, this.damageTotal, this.targets);
        await applyTokenDamage(this.damageDetail, this.damageTotal, this.targets, null, new Set())
        return super.next(WORKFLOWSTATES.ROLLFINISHED);

      default: return super._next(newState);
    }
  }
}

export class TrapWorkflow extends Workflow {

  trapSound: {playlist: string, sound: string};
  trapCenter: {x: number, y: number};
  saveTargets: any;

  constructor(actor: Actor5e, item: Item5e, targets: [Token], trapCenter: {x: number, y: number} = undefined, trapSound: {playlist: string , sound: string} = undefined,  event: any = null) {
    super(actor, item, null, ChatMessage.getSpeaker(), new Set(targets), event);
    // this.targets = new Set(targets);
    if (!this.event) this.event = duplicate(shiftOnlyEvent);
    this.trapSound = trapSound;
    this.trapCenter = trapCenter;
    this.saveTargets = new Set(game.user.targets);
    this.next(WORKFLOWSTATES.NONE)
    this.rollOptions.fastForward = true;
  }
  
  async _next(newState: number) {
    this.currentState = newState;
    let state = Object.entries(WORKFLOWSTATES).find(a=>a[1]===newState)[0];
    warn("attack workflow.next ", state, this._id, this)
    switch (newState) {
      case WORKFLOWSTATES.NONE:
        this.itemCardId = (await showItemCard.bind(this.item)(false, this, true))?.id;
        //@ts-ignore
        if (this.trapSound) AudioHelper.play({src: this.trapSound}, false)
        debug(" workflow.next ", state, this.item, configSettings.autoTarget, this.item.hasAreaTarget);
        // don't support the placement of a tempalte
        return this.next(WORKFLOWSTATES.AWAITTEMPLATE);

      case WORKFLOWSTATES.AWAITTEMPLATE:
        const targetDetails = this.item.data.data.target;
        if (configSettings.rangeTarget && targetDetails?.units === "ft" && ["creature", "ally", "enemy"].includes(targetDetails?.type)) {
          this.setRangedTargets(targetDetails)
          return this.next(WORKFLOWSTATES.TEMPLATEPLACED);
        }
        if (!this.item.hasAreaTarget || !this.trapCenter) return this.next(WORKFLOWSTATES.TEMPLATEPLACED)
        //@ts-ignore
        this.placeTemlateHookId = Hooks.once("createMeasuredTemplate", selectTargets.bind(this));
        const template = AbilityTemplate.fromItem(this.item);
        // template.draw();
        // get the x and y position from the trapped token
        template.data.x = this.trapCenter.x;
        template.data.y = this.trapCenter.y;
        // Create the template
        canvas.scene.createEmbeddedEntity("MeasuredTemplate", template.data);
        return;

      case WORKFLOWSTATES.TEMPLATEPLACED:
        // perhaps auto place template?
        return this.next(WORKFLOWSTATES.VALIDATEROLL);

      case WORKFLOWSTATES.VALIDATEROLL:
        // do pre roll checks
        return this.next(WORKFLOWSTATES.WAITFORATTACKROLL);

      case WORKFLOWSTATES.WAITFORATTACKROLL:
        if (!this.item.hasAttack) {
          this.hitTargets = new Set(this.targets);
          return this.next(WORKFLOWSTATES.WAITFORSAVES);
        }
        warn("attack roll ", this.event)
        this.item.rollAttack({event: this.event});
        return;

      case WORKFLOWSTATES.ATTACKROLLCOMPLETE:
        this.processAttackRoll();
        await this.displayAttackRoll(false, configSettings.mergeCard);
        await this.checkHits();
        const whisperCard = configSettings.autoCheckHit === "whisper" || game.settings.get("core", "rollMode") === "blindroll";
        await this.displayHits(whisperCard, configSettings.mergeCard);
        return this.next(WORKFLOWSTATES.WAITFORSAVES);

      case WORKFLOWSTATES.WAITFORDAMGEROLL:
        if (!this.item.hasDamage) return this.next(WORKFLOWSTATES.WAITFORSAVES);
        if (this.isFumble) {
          // fumble means no trap damage/effects
          return this.next(WORKFLOWSTATES.ROLLFINISHED);
        } 
        this.rollOptions.critical = this.isCritical;
        debug("Rolling damage ", this.event, this.itemLevel, this.rollOptions.versatile);
        this.rollOptions.critical = this.isCritical;
        this.rollOptions.fastForward = true;
        this.item.rollDamage(this.rollOptions);
        return; // wait for a damage roll to advance the state.

      case WORKFLOWSTATES.DAMAGEROLLCOMPLETE:
         if (!this.item.hasAttack) { // no attack roll so everyone is hit
          this.hitTargets = new Set(this.targets)
          warn(" damage roll complete for non auto target area effects spells", this)
        }
        // apply damage to targets plus saves plus immunities
        await this.displayDamageRoll(false, configSettings.mergeCard)
        if (this.isFumble) {
          return this.next(WORKFLOWSTATES.APPLYDYNAMICEFFECTS);
        }
        // If the item does damage, use the same damage type as the item
        let defaultDamageType = this.item?.data.data.damage?.parts[0][1] || this.defaultDamageType;
        this.damageDetail = createDamageList(this.damageRoll, this.item, defaultDamageType);
        return this.next(WORKFLOWSTATES.ALLROLLSCOMPLETE);

      case WORKFLOWSTATES.WAITFORSAVES:
        if (!this.item.hasSave) {
          this.saves = new Set(); // no saving throw, so no-one saves
          this.failedSaves = new Set(this.hitTargets);
          return this.next(WORKFLOWSTATES.WAITFORDAMGEROLL);
        }
        let hookId = Hooks.on("renderChatMessage", this.processSaveRoll.bind(this));
        let brHookId = Hooks.on("renderChatMessage", this.processBetterRollsChatCard.bind(this));
        try {
          await this.checkSaves(true);
        } finally {
          //@ts-ignore - does not support ids
          Hooks.off("renderChatMessage", hookId);
          //@ts-ignore does not support ids
          Hooks.off("renderChatMessage", brHookId);
        }
        //@ts-ignore ._hooks not defined
        debug("Check Saves: renderChat message hooks length ", Hooks._hooks["renderChatMessage"]?.length)
        this.displaySaves(configSettings.autoCheckSaves === "whisper", configSettings.mergeCard);
        return this.next(WORKFLOWSTATES.SAVESCOMPLETE);

      case WORKFLOWSTATES.SAVESCOMPLETE:
        return this.next(WORKFLOWSTATES.WAITFORDAMGEROLL);
  
      case WORKFLOWSTATES.ALLROLLSCOMPLETE:
        debug("all rolls complete ", this.damageDetail)
        if (this.damageDetail.length) processDamageRoll(this, this.damageDetail[0].type)
        return this.next(WORKFLOWSTATES.APPLYDYNAMICEFFECTS);

      case WORKFLOWSTATES.ROLLFINISHED:
      // area effect trap, put back the targets the way they were
      if (this.saveTargets && this.item.hasAreaTarget) {
          game.user.targets.forEach(t => {
            //@ts-ignore
            t.setTarget(false, { releaseOthers: false });
          });
          game.user.targets.clear();
          this.saveTargets.forEach(t => {
            t.setTarget(true, {releaseOthers: false})
            game.user.targets.add(t)
          })
        }
        return super._next(WORKFLOWSTATES.ROLLFINISHED);

      default:
        return super._next(newState);
    }
  }
}

export class BetterRollsWorkflow extends Workflow {
  betterRollsHookId: number;
  static get(id:string):BetterRollsWorkflow {
    return Workflow._workflows[id];
  }
  get isBetterRollsWorkflow() {return true};

  async _next(newState) {
    this.currentState = newState;
    let state = Object.entries(WORKFLOWSTATES).find(a=>a[1]===this.currentState)[0];
    warn("betterRolls workflow.next ", state, configSettings.speedItemRolls, this)
    switch (newState) {
      case WORKFLOWSTATES.WAITFORATTACKROLL:
        // since this is better rolls as soon as we are ready for the attack roll we have both the attack roll and damage
        if (!this.item.hasAttack) {
          return this.next(WORKFLOWSTATES.WAITFORDAMGEROLL);
        }
        return this.next(WORKFLOWSTATES.ATTACKROLLCOMPLETE);

      case WORKFLOWSTATES.ATTACKROLLCOMPLETE:
        debug(this.attackRollHTML)
        if (configSettings.autoCheckHit !== "none") {
          await this.checkHits();
          await this.displayHits(configSettings.autoCheckHit === "whisper", configSettings.mergeCard);
        }
        Hooks.callAll("midi-qol.AttackRollComplete", this);
        return this.next(WORKFLOWSTATES.WAITFORDAMGEROLL);

      case WORKFLOWSTATES.WAITFORDAMGEROLL:
        // better rolls always have damage rolled
        if (!this.item.hasDamage) return this.next(WORKFLOWSTATES.WAITFORSAVES);
        else return this.next(WORKFLOWSTATES.DAMAGEROLLCOMPLETE);

      case WORKFLOWSTATES.DAMAGEROLLCOMPLETE:
        if (configSettings.autoTarget === "none" && this.item.hasAreaTarget && !this.item.hasAttack) { 
          // we are not auto targeting so for area effect attacks, without hits (e.g. fireball)
          this.targets = new Set(game.user.targets);
          this.hitTargets = new Set(game.user.targets);
       }
        // apply damage to targets plus saves plus immunities
        if (this.isFumble) { //TODO: Is this right?
          return this.next(WORKFLOWSTATES.ROLLFINISHED);
        }
        if (this.item.hasSave) return this.next(WORKFLOWSTATES.WAITFORSAVES)
        processDamageRoll(this, "psychic");
        Hooks.callAll("midi-qol.DamageRollComplete", this)
        return this.next(WORKFLOWSTATES.APPLYDYNAMICEFFECTS);

      default: 
        return await super._next(newState);
    }
  }
}

